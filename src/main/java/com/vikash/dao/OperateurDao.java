package com.vikash.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vikash.modal.ExploitantModel;
import com.vikash.modal.OperateurModel;
import com.vikash.repository.OperateurRepository;


@Service
public class OperateurDao {
	
	@Autowired
	OperateurRepository operateurRepository;
	
	//Ajout
	public OperateurModel save(OperateurModel om) {
		return operateurRepository.save(om);
	}
	
	//recherche
	public List<OperateurModel> findAll(){
		return operateurRepository.findAll();
	}	
	
	//get by id
	public OperateurModel findOne(Integer id) {
		return operateurRepository.findOne(id);
	}
	
	//supprimer
	public void delete(OperateurModel om) {
		operateurRepository.delete(om);
	}
	
}
