package com.vikash.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vikash.dao.OperateurDao;
import com.vikash.modal.OperateurModel;

@RestController
@RequestMapping("exploit/operateur/")
public class OperateurController {

	@Autowired
	OperateurDao operateurDao;
	
	@Autowired
	public OperateurController() {
		// TODO Auto-generated constructor stub
	}
	
	//Enregistrement
	@PostMapping("/add_expl")
	public OperateurModel createOperateur(@Valid @RequestBody OperateurModel om ) {
		
		return operateurDao.save(om);
	}
	
	//affiche operateur
	@RequestMapping(value="/show_expl" , method=RequestMethod.GET)
	public String exploitant(ModelMap model) {
		
		OperateurModel om = new OperateurModel();
		model.addAttribute("om", operateurDao.findAll());
		
		return "listeExploitant";
	}
	
	//affiche par ID
	@GetMapping("/show_expl_id/{id}")
	public ResponseEntity<OperateurModel> getById(@PathVariable(value="id") int id){
		
		OperateurModel om=operateurDao.findOne(id);
		
		if (om==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(om);
	}
	
	//update by id
	@PutMapping("/update_expl/{id}")
	public ResponseEntity<OperateurModel> updateOperateur(@PathVariable(value="id") int id, @Valid @RequestBody OperateurModel omp){
		
		OperateurModel om= operateurDao.findOne(id);
		
		if (om==null) {
			return ResponseEntity.notFound().build();
		}
		
		om.setNOMEXPLOITANT(omp.getNOMEXPLOITANT());
		om.setPRENOMEXPL(omp.getPRENOMEXPL());
		om.setADRESSE(omp.getADRESSE());
		om.setNUMTEL(omp.getNUMTEL());
		om.setNUMCIN(omp.getNUMCIN());
		om.setSEXE(omp.getSEXE());
		om.setDATENAISS(omp.getDATENAISS());
		om.setCOMMUNE(omp.getCOMMUNE());
		
		OperateurModel updateOperateur=operateurDao.save(om);
		return ResponseEntity.ok().body(updateOperateur);
	}
	
	//delete operateur
	@DeleteMapping("/delete_expl/{id}")
	public ResponseEntity<OperateurModel> deleteOperateur(@PathVariable(value="id") int id){
		
		OperateurModel om=operateurDao.findOne(id);
		if (om==null) {
			return ResponseEntity.notFound().build();
		}
		
		operateurDao.delete(om);
		return ResponseEntity.ok().build();
	}
	
}
