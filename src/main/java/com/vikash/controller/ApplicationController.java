package com.vikash.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.vikash.dao.OperateurDao;
import com.vikash.modal.OperateurModel;
import com.vikash.repository.OperateurRepository;



@Controller

public class ApplicationController {

	
	@Autowired
	private OperateurDao operateurDao;
	
	@Autowired
	OperateurRepository operateurRepository;
	
	@RequestMapping("/dashboard")
	public String Welcome(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "index";
	}

	@RequestMapping(value="/exploitant" , method=RequestMethod.GET)
	public String exploitant(ModelMap model) {
		List<OperateurModel> list = operateurRepository.findAll();
		model.addAttribute("list",list);		
		return "listeExploitant";
	}

	@RequestMapping("/collecteur")
	public String collecteur(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "listeCollecteur";
	}

	@RequestMapping("/action")
	public String action(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "listeaction";
	}

	@RequestMapping("/perception")
	public String perception(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "gestionperception";
	}
	
	@RequestMapping("/exportation")
	public String exportation(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "exportation";
	}
	
	@RequestMapping("/user")
	public String user(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "user";
	}

	@RequestMapping(value="/user",method=RequestMethod.POST,consumes = {"application/json"} )
	public String userCreate(@Valid @RequestBody OperateurModel om) {
		operateurRepository.save(om);
		
		return "listeExploitant";
	}

//	@RequestMapping(value="/user")
//	public String saveRegistration(@Valid OperateurModel om,
//			BindingResult result,
//			ModelMap model,
//			RedirectAttributes redirectAttributes) {
//		
//		if(result.hasErrors()) {
//			return "/user";
//		}
//		
//		operateurDao.save(om);
//		
//		return "redirect:/exploitant";
//	}
	
//	@PostMapping("/user")
//	public OperateurModel createOperateur(@Valid @RequestBody OperateurModel om) {
//		return operateurDao.save(om);
//	}
//

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "deconnection";
	}

}
